# Resource Calendar Discovery

Calendar discovers the resource availability from event logs.

## Running the Application
- Download the application or clone the code.
- Application is a `console Java application`
- You can import the code into your IDE (Eclipse or Intelli) or run it from command line.
- Once the application starts it asks for a log file path and threshold values for `Support` and `Confidence`
- BIMP Calendar will be generated and printed into the command line
- A full detailed logs for each step of the Calendar is generated under [/logs_meta](/logs_meta)

![](.README_images/fdc779bb.png)