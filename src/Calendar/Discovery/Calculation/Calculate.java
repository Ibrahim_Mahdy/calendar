package Calendar.Discovery.Calculation;

import Calendar.Discovery.LogDiscovery.CalendarExpression;
import Calendar.Discovery.LogDiscovery.Period;
import Calendar.Discovery.SimplePatternsGeneration.SimpleCalendarPattern;
import lombok.Data;

import java.util.*;
import java.util.stream.Collectors;


@Data
public class Calculate {
    private List<SimpleCalendarPattern> logCalculatedSimpleCalendarPatterns;

    public Calculate(double supportThreshold, double confidenceThreshold, List<CalendarExpression> logCalendarExpressions, List<SimpleCalendarPattern> logSimpleCalendarPatterns, List<Period> logPeriods) {
        List<SimpleCalendarPattern> logSps = new ArrayList<>(logSimpleCalendarPatterns);
        logCalculatedSimpleCalendarPatterns = calculate(supportThreshold, confidenceThreshold, logCalendarExpressions, logSps, logPeriods);
    }

    private List<SimpleCalendarPattern> calculate(double supportThreshold, double confidenceThreshold, List<CalendarExpression> logCalendarExpressions, List<SimpleCalendarPattern> logSps, List<Period> logPeriods) {
        double spSupport;
        double spConfidence;
        List<SimpleCalendarPattern> failedSimpleCalendarPatterns = new ArrayList<>();
        int logCalendarExpressionsSize = logCalendarExpressions.size();

        for (Iterator<SimpleCalendarPattern> iter = logSps.listIterator(); iter.hasNext(); ) {
            SimpleCalendarPattern sp = iter.next();
            SimpleCalendarPattern spFailed = failedSimpleCalendarPatterns.stream()
                    .filter(mySp -> (mySp.getYear() == null || mySp.getYear().equals(sp.getYear())) && (mySp.getMonth() == null || mySp.getMonth().equals(sp.getMonth())) && mySp.getWeek().equals(sp.getWeek()) && mySp.getDayOfWeek().equals(sp.getDayOfWeek()) && mySp.getHour().equals(sp.getHour()))
                    .findAny()
                    .orElse(null);

            if (spFailed != null) {
                iter.remove();
                continue;
            }

            List<CalendarExpression> existCalendarExpressions = (List<CalendarExpression>) filterBySp(logCalendarExpressions, sp);
            List<SimpleCalendarPattern> existSimpleCalendarPatterns = (List<SimpleCalendarPattern>) filterBySp(logSps, sp);

            // support
            spSupport = ((double) existCalendarExpressions.size() / logCalendarExpressionsSize) * 100;
            sp.setSupport(spSupport);

            // remove items with support < threshold
            if (spSupport < supportThreshold) {
                failedSimpleCalendarPatterns.addAll(existSimpleCalendarPatterns);
                iter.remove();
                continue;
            }

            Set<CalendarExpression> nonDuplicatedCE = new HashSet<>(existCalendarExpressions);
            //confidence
            spConfidence = calculateConfidence(sp, nonDuplicatedCE, logPeriods) * 100;
            sp.setConfidence(spConfidence);

            // remove items with confidence < threshold
            if (spConfidence < confidenceThreshold) {
                iter.remove();
            }
        }

        return logSps;
    }

    private List<? extends CalendarExpression> filterBySp(List<? extends CalendarExpression> listToFilter, SimpleCalendarPattern sp){
        if(sp.getYear() != null && !sp.getYear().equals("*")){
            listToFilter = listToFilter.stream().filter(p -> p.getYear().equals(sp.getYear())).collect(Collectors.toList());
        }
        if(sp.getMonth() != null && !sp.getMonth().equals("*")){
            listToFilter = listToFilter.stream().filter(p -> p.getMonth().equals(sp.getMonth())).collect(Collectors.toList());
        }
        if(sp.getWeek() != null && !sp.getWeek().equals("*")){
            listToFilter = listToFilter.stream().filter(p -> p.getWeek().equals(sp.getWeek())).collect(Collectors.toList());
        }

        if(sp.getDayOfWeek() != null && !sp.getDayOfWeek().equals("*")){
            listToFilter = listToFilter.stream().filter(p -> p.getDayOfWeek().equals(sp.getDayOfWeek())).collect(Collectors.toList());
        }
        if(sp.getHour() != null && !sp.getHour().equals("*")){
            listToFilter = listToFilter.stream().filter(p -> p.getHour().equals(sp.getHour())).collect(Collectors.toList());
        }
        return listToFilter;
    }

    private double calculateConfidence(SimpleCalendarPattern sp, Set<CalendarExpression> nonDuplicatedCE, List<Period> logPeriod) {
        long periodCalendarExpressionsCount = 0;
        int monthUnfolded, weekUnfolded, dayUnfolded, hourUnfolded;
        monthUnfolded = weekUnfolded = dayUnfolded = hourUnfolded = 1;
        double spConfidence = 0;

        for (Period myPeriod : logPeriod) {
            if (sp.getYear() != null && sp.getYear().equals("*")) {
                periodCalendarExpressionsCount = nonDuplicatedCE.stream().filter(p -> p.getYear().equals(Integer.toString(myPeriod.getYear()))).count();
            }

            if (sp.getWeek() != null && sp.getWeek().equals("*")) {
                weekUnfolded = (myPeriod.getToWeek() - myPeriod.getFromWeek() + 1);
            }

            if (sp.getDayOfWeek() != null && sp.getDayOfWeek().equals("*")) {
                dayUnfolded = (myPeriod.getToDayOfWeek() - myPeriod.getFromDayOfWeek() + 1);
            } else if (sp.getDayOfMonth() != null && sp.getDayOfMonth().equals("*")) {
                dayUnfolded = (myPeriod.getToDayOfMonth() - myPeriod.getFromDayOfMonth() + 1);
            }

            if (sp.getHour() != null && sp.getHour().equals("*")) {
                hourUnfolded = (myPeriod.getToHour() - myPeriod.getFromHour());
            }
            double unfoldedSize = monthUnfolded * weekUnfolded * dayUnfolded * hourUnfolded;
            spConfidence += (periodCalendarExpressionsCount / unfoldedSize);
        }

        return spConfidence / logPeriod.size();
    }
}

