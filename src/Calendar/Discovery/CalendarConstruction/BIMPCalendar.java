package Calendar.Discovery.CalendarConstruction;

import lombok.Data;

import java.util.List;

@Data
public class BIMPCalendar {
    private String createdBIMPCalendar;

    public BIMPCalendar(List<CalendarModel> calendarList) {
        this.createdBIMPCalendar = createBIMPCalendar(calendarList);
    }

    public String createBIMPCalendar(List<CalendarModel> calendarList) {
        StringBuilder rules = new StringBuilder();
        for (CalendarModel calendar: calendarList) {
            rules.append("         <qbp:rule fromTime=\"").append(calendar.getFromTime()).append("\" toTime=\"").append(calendar.getToTime()).append("\" fromWeekDay=\"").append(calendar.getFromWeekDay()).append("\" toWeekDay=\"").append(calendar.getToWeekDay()).append("\"/> \n");
        }
        return
                "    <qbp:timetable id=\"CustomTimetable\" default=\"false\" name=\"DiscoveredTimeTable\">\n" +
                "      <qbp:rules>\n" +
                             rules.toString() +
                "      </qbp:rules>\n" +
                "    </qbp:timetable>\n";
    }
}
