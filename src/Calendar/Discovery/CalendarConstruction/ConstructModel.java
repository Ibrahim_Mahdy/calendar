package Calendar.Discovery.CalendarConstruction;

import Calendar.Discovery.SimplePatternsGeneration.SimpleCalendarPattern;
import lombok.Data;

import java.util.List;
import java.util.Set;

@Data
public class ConstructModel {

    private int dayOfWeek;
    private List<SimpleCalendarPattern> dayOfWeekSPs;
    private Set<Integer> hoursOfDay;
    private List<List<Integer>> listOfHoursOfDay;


    public ConstructModel(int dayOfWeek, List<SimpleCalendarPattern> dayOfWeekSPs, Set<Integer> hoursOfDay, List<List<Integer>> listOfHoursOfDay) {
        this.dayOfWeek = dayOfWeek;
        this.dayOfWeekSPs = dayOfWeekSPs;
        this.hoursOfDay = hoursOfDay;
        this.listOfHoursOfDay = listOfHoursOfDay;
    }
}
