package Calendar.Discovery.CalendarConstruction;

import Calendar.Discovery.SimplePatternsGeneration.SimpleCalendarPattern;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;
import java.util.stream.Collectors;

@Data
public class Construct {
    List<ConstructModel> dayOfWeekHours;

    public Construct(List<SimpleCalendarPattern> calculated) {
        dayOfWeekHours = dayOfWeek(calculated);
    }

    private List<ConstructModel> dayOfWeek(List<SimpleCalendarPattern> calculated){
        if(calculated.get(0).getDayOfWeek() == null) return null;

        List<ConstructModel> calendarModels = new ArrayList<>();
        for(int day = 1; day<= 7; day++){
            List<SimpleCalendarPattern> myDayOfWeek = filterForDayOfWeek(calculated, day);
            if(myDayOfWeek.size() > 0){
                Set<Integer> hoursSet = getHoursOfDay(myDayOfWeek);
                List<List<Integer>> myList = listOfHours(hoursSet);
                calendarModels.add(new ConstructModel(day, myDayOfWeek, hoursSet, myList));
            }
        }

        return calendarModels;
    }


    private List<SimpleCalendarPattern> filterForDayOfWeek(List<SimpleCalendarPattern> listToFilter, int dayOfWeek){
        return listToFilter.stream().filter(p -> p.getWeek().equals("*") && !p.getDayOfWeek().equals("*") && Integer.parseInt(p.getDayOfWeek()) == dayOfWeek && !p.getHour().equals("*")).collect(Collectors.toList());
    }

    private Set<Integer> getHoursOfDay(List<SimpleCalendarPattern> myDayOfWeekList) {
        Set<Integer> myHoursOfDay = new TreeSet<Integer>();
        for (SimpleCalendarPattern mySp: myDayOfWeekList) {
            myHoursOfDay.add(Integer.parseInt(mySp.getHour()));
        }
        return myHoursOfDay;
    }


    private List<List<Integer>> listOfHours(Set<Integer> hoursOfDay){
        List<Integer> listOfHoursOfDay = new ArrayList<Integer>( hoursOfDay );
        List<List<Integer>> myList = new ArrayList<>();
        List<Integer> consecutiveHours = new ArrayList<>();

        for(int index = 0; index < listOfHoursOfDay.size(); index++){
            if(index != 0 && listOfHoursOfDay.get(index) - listOfHoursOfDay.get(index-1) != 1){
                myList.add(consecutiveHours);
                consecutiveHours = new ArrayList<>();
            }
            consecutiveHours.add(listOfHoursOfDay.get(index));
        }

        if(consecutiveHours.size() > 0){
            myList.add(consecutiveHours);
        }

        return myList;
    }






}
