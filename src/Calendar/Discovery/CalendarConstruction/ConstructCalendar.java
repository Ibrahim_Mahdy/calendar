package Calendar.Discovery.CalendarConstruction;

import lombok.Data;

import java.time.DayOfWeek;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

@Data
public class ConstructCalendar {
    private List<CalendarModel> dayOfWeekCalendar;

    public ConstructCalendar(List<ConstructModel> constructModel) {
       this.dayOfWeekCalendar = createCalendar(constructModel);
    }

    public List<CalendarModel> createCalendar(List<ConstructModel> constructModel){
        List<CalendarModel> calendarModels = new ArrayList<>();
        for (ConstructModel model: constructModel) {
            for (List<Integer> hours: model.getListOfHoursOfDay()) {
                String day = DayOfWeek.of(model.getDayOfWeek()).toString();
                LocalTime fromTime =  LocalTime.of(hours.get(0), 0, 0, 0);
                LocalTime toTime =  LocalTime.of(hours.get(hours.size()-1), 59, 0, 0);

                calendarModels.add(new CalendarModel(day,day, fromTime , toTime));
            }
        }

        return calendarModels;
    }
}
