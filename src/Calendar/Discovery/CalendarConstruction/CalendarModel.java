package Calendar.Discovery.CalendarConstruction;

import lombok.Data;

import java.time.LocalTime;

@Data
public class CalendarModel {
    String fromWeekDay;
    String toWeekDay;

    LocalTime fromTime;
    LocalTime toTime;

    public CalendarModel(String fromWeekDay, String toWeekDay, LocalTime fromTime, LocalTime toTime) {
        this.fromWeekDay = fromWeekDay;
        this.toWeekDay = toWeekDay;
        this.fromTime = fromTime;
        this.toTime = toTime;
    }
}
