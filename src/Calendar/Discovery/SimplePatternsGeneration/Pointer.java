package Calendar.Discovery.SimplePatternsGeneration;

import Calendar.Discovery.LogDiscovery.GranularityExpression;
import lombok.Data;

import java.util.List;

@Data
class Pointer {

    private int currentIndex;
    private int size;
    private List<String> myList;
    private GranularityExpression granularityExpression;

    Pointer(List<String> myList, GranularityExpression myCandidateType) {
        this.currentIndex = 0;
        this.size = myList.size();
        this.myList = myList;
        this.granularityExpression = myCandidateType;
    }
}

