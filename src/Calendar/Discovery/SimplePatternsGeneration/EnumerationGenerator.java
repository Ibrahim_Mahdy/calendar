package Calendar.Discovery.SimplePatternsGeneration;

import Calendar.Discovery.LogDiscovery.Candidates;
import lombok.Data;

import java.util.LinkedList;
import java.util.List;

@Data
public class EnumerationGenerator {

    private List<SimpleCalendarPattern> logSimpleCalendarPatterns;

    public EnumerationGenerator(List<Candidates> logCandidates) {
        logSimpleCalendarPatterns = generateSimpleCalendarPatterns(logCandidates);
    }

    private List<SimpleCalendarPattern> generateSimpleCalendarPatterns(List<Candidates> logCandidates) {

        boolean hasMorePatterns= true;

        List<Pointer> pointer = new LinkedList<>();
        for (Candidates logCand: logCandidates) {
            Pointer myPointer = new Pointer(logCand.getCandidate(), logCand.getGranularityExpression());
            pointer.add(myPointer);
        }

        SimpleCalendarPattern simpleCalendarPattern;
        List<SimpleCalendarPattern> simpleCalendarPatternList = new LinkedList<>();

        while(hasMorePatterns){
            simpleCalendarPattern = new SimpleCalendarPattern();
            for (Pointer p: pointer) {
                String value = p.getMyList().get(p.getCurrentIndex());
                switch(p.getGranularityExpression()) {
                    case YEAR:
                        simpleCalendarPattern.setYear(value);
                        break;
                    case MONTH:
                        simpleCalendarPattern.setMonth(value);
                        break;
                    case WEEK:
                        simpleCalendarPattern.setWeek(value);

                    case DAY_OF_MONTH:
                        simpleCalendarPattern.setDayOfWeek(value);
                        break;
                    case DAY_OF_WEEK:
                        simpleCalendarPattern.setDayOfWeek(value);
                    case HOUR_OF_DAY:
                        simpleCalendarPattern.setHour(value);
                        break;
                    default:
                }
            }
            simpleCalendarPatternList.add(simpleCalendarPattern);

            // update pointer position
            // move index of first list and break. until first list is looped through, then set it to first element and increase index of next list by one
            for (Pointer p: pointer) {
                if (p.getCurrentIndex() >= (p.getSize()-1)) {
                    p.setCurrentIndex(0);
                    hasMorePatterns = false;
                } else {
                    p.setCurrentIndex(p.getCurrentIndex()+1);
                    hasMorePatterns = true;
                    break;
                }
            }
        }
        return simpleCalendarPatternList;
    }
}
