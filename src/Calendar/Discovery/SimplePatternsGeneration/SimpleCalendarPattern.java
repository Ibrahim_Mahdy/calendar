package Calendar.Discovery.SimplePatternsGeneration;

import Calendar.Discovery.LogDiscovery.CalendarExpression;
import lombok.Data;

@Data
public class SimpleCalendarPattern extends CalendarExpression {
    private double support;
    private double confidence;

    @Override
    public String toString() {
        String spYear = getYear() == null? "" : "Year=" + getYear() + ", ";
        String spMonth = getMonth() == null? "" : "Month=" + getMonth() + ", ";
        String spWeek = getWeek() == null? "" : "Week=" + getWeek() + ", ";
        String spDayPfWeek = getDayOfWeek() == null? "" : "DayOfWeek=" + getDayOfWeek() + ", ";
        String spDayOfMonth = getDayOfMonth() == null? "" : "DayOfMonth=" + getDayOfMonth() + ", ";
        String spHour = getHour() == null? "" : "Hour=" + getHour() + ", ";

        return "SimpleCalendarPattern: {" +
                spYear +
                spMonth +
                spWeek +
                spDayPfWeek +
                spDayOfMonth +
                spHour +
                "Support=" + support + ", " +
                "Confidence=" + confidence + "}";
    }
}
