package Calendar.Discovery.LogMetadata;

import Calendar.Discovery.CalendarConstruction.CalendarModel;
import Calendar.Discovery.CalendarConstruction.ConstructModel;
import Calendar.Discovery.LogDiscovery.*;
import Calendar.Discovery.SimplePatternsGeneration.SimpleCalendarPattern;

import java.io.File;
import java.io.FileWriter;
import java.util.List;

public class CreateMetadata {

    private FileWriter myWriter;
    private int fileCount;
    private String logDirectory;

    public CreateMetadata(String logName, String userInput, List<GranularityExpression> logGranularityExpressions, LogInfo logInfo, List<SimpleCalendarPattern> simpleCalendarPatterns, List<SimpleCalendarPattern> calculatedSP, List<ConstructModel> dayOfWeekHours, List<CalendarModel> dayOfWeekCalendar, String createdBIMPCalendar) {
        fileCount = 0;
        logDirectory = "logs_meta/" + logName;
        new File(logDirectory).mkdirs();

        writeUserInputs(userInput);
        writeGranularityExpressions(logGranularityExpressions);
        writeLogCalendarExpressions(logInfo.getLogCalendarExpressions());
        writeLogCandidates(logInfo.getLogCandidates());
        writeLogPeriod(logInfo.getLogPeriods());
        writeSimpleCalendarExpressions(simpleCalendarPatterns, "Discovered_SimpleCalendarPatterns");
        writeSimpleCalendarExpressions(calculatedSP, "Calculated_SimpleCalendarPatterns");
        writeDayOfWeekHoursList(dayOfWeekHours);
        writeDayOfWeekCalendar(dayOfWeekCalendar);
        writeCreatedBIMPCalendar(createdBIMPCalendar);
    }

    private void writeUserInputs(String userInput) {
        try {
            myWriter = new FileWriter(logDirectory + "/" + fileCount + "_UserInput.txt");
            myWriter.write(userInput);
            myWriter.close();
            fileCount++;
        } catch (Exception e) {
            System.out.println("Error Writing User Input Meta Data: " + e.getMessage());
        }
    }

    private void writeGranularityExpressions(List<GranularityExpression> logGranularityExpressions) {
        try {
            myWriter = new FileWriter(logDirectory + "/" + fileCount + "_GranularityExpressions.txt");
            for (GranularityExpression ge : logGranularityExpressions) {
                myWriter.write(ge.toString() + System.lineSeparator());
            }
            myWriter.close();
            fileCount++;
        } catch (Exception e) {
            System.out.println("Error Writing Granularity Expressions Meta Data: " + e.getMessage());
        }
    }


    private void writeLogCalendarExpressions(List<CalendarExpression> logCalendarExpressions) {
        try {
            myWriter = new FileWriter(logDirectory + "/" + fileCount + "_LogCalendarExpressions.txt");
            for (CalendarExpression ce : logCalendarExpressions) {
                myWriter.write(ce.toString() + System.lineSeparator());
            }
            myWriter.close();
            fileCount++;
        } catch (Exception e) {
            System.out.println("Error Writing Calendar Expressions Meta Data: " + e.getMessage());
        }
    }

    private void writeLogCandidates(List<Candidates> logCandidates) {
        try {
            myWriter = new FileWriter(logDirectory + "/" + fileCount + "_LogCandidates.txt");
            for (Candidates candidate : logCandidates) {
                myWriter.write(candidate.toString() + System.lineSeparator() + System.lineSeparator());
            }
            myWriter.close();
            fileCount++;
        } catch (Exception e) {
            System.out.println("Error Writing Candidates Meta Data: " + e.getMessage());
        }
    }

    private void writeLogPeriod(List<Period> logPeriods) {
        try {
            myWriter = new FileWriter(logDirectory + "/" + fileCount + "_LogPeriods.txt");
            for (Period period : logPeriods) {
                myWriter.write(period.toString() + System.lineSeparator() + System.lineSeparator());
            }
            myWriter.close();
            fileCount++;
        } catch (Exception e) {
            System.out.println("Error Writing Log Periods Meta Data: " + e.getMessage());
        }
    }

    private void writeSimpleCalendarExpressions(List<SimpleCalendarPattern> simpleCalendarPatterns, String fileName) {
        try {
            myWriter = new FileWriter(logDirectory + "/" + fileCount + "_" + fileName + ".txt");
            for (SimpleCalendarPattern sp : simpleCalendarPatterns) {
                myWriter.write(sp.toString() + System.lineSeparator());
            }
            myWriter.close();
            fileCount++;
        } catch (Exception e) {
            System.out.println("Error Writing" + fileName + " Meta Data: " + e.getMessage());
        }
    }


    private void writeDayOfWeekHoursList(List<ConstructModel> dayOfWeekHours) {
        try {
            myWriter = new FileWriter(logDirectory + "/" + fileCount + "_dayOfWeekHoursList.txt");
            for (ConstructModel myDayOfWeekHours : dayOfWeekHours) {
                for (SimpleCalendarPattern myDay: myDayOfWeekHours.getDayOfWeekSPs()) {
                    myWriter.write(myDay.toString() + System.lineSeparator());
                }
                myWriter.write(System.lineSeparator());
                myWriter.write("Day: " + myDayOfWeekHours.getDayOfWeek() + ", Hours: " + myDayOfWeekHours.getHoursOfDay() + System.lineSeparator());
                myWriter.write("Consecutive hours: " + myDayOfWeekHours.getListOfHoursOfDay() + System.lineSeparator());
                myWriter.write("-----------------------------------------------------------------" + System.lineSeparator() + System.lineSeparator());
            }
            myWriter.close();
            fileCount++;
        } catch (Exception e) {
            System.out.println("Error Writing Day Of Week Hours List Meta Data: " + e.getMessage());
        }
    }

    private void writeDayOfWeekCalendar(List<CalendarModel> dayOfWeekCalendar) {
        try {
            myWriter = new FileWriter(logDirectory + "/" + fileCount + "_dayOfWeekCalendar.txt");
            for (CalendarModel myDayOfWeekCalendar : dayOfWeekCalendar) {
                myWriter.write(myDayOfWeekCalendar.toString() + System.lineSeparator());
            }
            myWriter.close();
            fileCount++;
        } catch (Exception e) {
            System.out.println("Error Writing Day Of Week Calendar Meta Data: " + e.getMessage());
        }
    }

    private void writeCreatedBIMPCalendar(String createdBIMPCalendar) {
        try {
            myWriter = new FileWriter(logDirectory + "/" + fileCount + "_CreatedBIMPCalendar.txt");
            myWriter.write(createdBIMPCalendar);
            myWriter.close();
            fileCount++;
        } catch (Exception e) {
            System.out.println("Error Writing CreatedBIMPCalendar Meta Data: " + e.getMessage());
        }
    }
}
