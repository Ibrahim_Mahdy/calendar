package Calendar.Discovery.LogDiscovery;

public enum GranularityExpression {
    YEAR,
    MONTH,
    WEEK,
    DAY_OF_MONTH,
    DAY_OF_WEEK,
    HOUR_OF_DAY,
    QUARTER_OF_HOUR,
}