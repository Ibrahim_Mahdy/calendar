package Calendar.Discovery.LogDiscovery;

import org.deckfour.xes.in.XUniversalParser;
import org.deckfour.xes.model.*;

import java.io.File;
import java.io.IOException;
import java.util.*;

public class ParseLog {
    
    public File readLog(String fileName) {
        try{
            File myLog =  new File(fileName);
            if(!myLog.exists()){
                throw new IOException("Could not find or read the file at the provided path!");
            }

            return myLog;
        } catch (Exception e){
            System.out.println("Error: " + e.getMessage());
            return null;
        }
    }

    public List<XTrace> parseLog(File log) {
        XUniversalParser parser = new XUniversalParser();
        if (parser.canParse(log)) {
            try {
                Collection<XLog> myLog = parser.parse(log);
                if (myLog.size() != 0) {
                    return myLog.iterator().next();
                }
            } catch (Exception e) {
                System.out.println("Error Parsing the file!: " + e.getMessage());
            }
        }
        return null;
    }

}
