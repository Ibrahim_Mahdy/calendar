package Calendar.Discovery.LogDiscovery;

import lombok.Data;
import org.deckfour.xes.model.XAttribute;
import org.deckfour.xes.model.XAttributeTimestamp;
import org.deckfour.xes.model.XEvent;
import org.deckfour.xes.model.XTrace;

import java.util.*;

@Data
public class LogInfo {
    private List<CalendarExpression> logCalendarExpressions;
    private List<Candidates> logCandidates;
    private List<Period> logPeriods;

    public LogInfo(List<XTrace> traces, List<GranularityExpression> logGranularityExpressions) {
        extractLogInfo(traces, logGranularityExpressions);
    }

    private void extractLogInfo(List<XTrace> traces, List<GranularityExpression> logGranularityExpressions) {
        logCalendarExpressions = new ArrayList<>();
        logCandidates = new ArrayList<>();

        // Set Log Periods:
        Period period = new Period();
        logPeriods = new ArrayList<>();

        List<Integer> logMonths = new ArrayList<>();
        List<Integer> logWeeks = new ArrayList<>();
        List<Integer> logDaysOfWeek = new ArrayList<>();
        List<Integer> logDaysOfMonth = new ArrayList<>();
        List<Integer> logHoursOfDay  = new ArrayList<>();

        Set<String> yearCandidates = new TreeSet<String>();
        Set<String> monthCandidates = new TreeSet<String>();
        Set<String> weekCandidates = new TreeSet<String>();
        Set<String> dayOfMonthCandidates = new TreeSet<String>();
        Set<String> dayOfWeekCandidates = new TreeSet<String>();
        Set<String> hourOfDayCandidates  = new TreeSet<String>();

        // Extract all events
        List<XEvent> allEvents = new ArrayList<>();
        for (XTrace myTrace : traces) {
            allEvents.addAll(myTrace);
        }

        // Sort Events by timestamp
        if (!allEvents.isEmpty()) {
            sortEvents(allEvents);
        }

        for (XEvent myEvent : allEvents) {
            for (Map.Entry<String, XAttribute> eAtt : myEvent.getAttributes().entrySet()) {
                if (eAtt.getValue() instanceof XAttributeTimestamp) {
                    Calendar calendar = new GregorianCalendar();
                    calendar.setTime(((XAttributeTimestamp) eAtt.getValue()).getValue());

                    int ceYear = calendar.get(Calendar.YEAR);
                    if (period.getYear() == 0) {
                        period.setYear(ceYear);
                    } else if (period.getYear() != 0 && period.getYear() != ceYear) {
                        setPeriod(period, logMonths, logWeeks, logDaysOfWeek, logDaysOfMonth, logHoursOfDay);

                        logMonths = new ArrayList<>();
                        logWeeks = new ArrayList<>();
                        logDaysOfWeek = new ArrayList<>();
                        logDaysOfMonth = new ArrayList<>();
                        logHoursOfDay = new ArrayList<>();

                        period = new Period();
                        period.setYear(ceYear);
                    }

                    int ceMonth = calendar.get(Calendar.MONTH);
                    logMonths.add(ceMonth);

                    int ceWeek = calendar.get(Calendar.WEEK_OF_YEAR);
                    logWeeks.add(ceWeek);

                    int ceDayOfMonth = calendar.get(Calendar.DAY_OF_MONTH);
                    logDaysOfMonth.add(ceDayOfMonth);

                    int ceDayOfWeek = calendar.get(Calendar.DAY_OF_WEEK);
                    logDaysOfWeek.add(ceDayOfWeek);

                    int ceHour = calendar.get(Calendar.HOUR_OF_DAY);
                    logHoursOfDay.add(ceHour);

                    // Set Calendar Expressions
                    String year = String.valueOf(ceYear);
                    String month = String.valueOf(ceMonth);
                    String week = String.valueOf(ceWeek);
                    String dayOfMonth = String.valueOf(ceDayOfMonth);
                    String dayOfWeek = String.valueOf(ceDayOfWeek);
                    String hourOfDay = String.valueOf(ceHour);

                    logCalendarExpressions.add(new CalendarExpression(year, month, week, dayOfMonth, dayOfWeek, hourOfDay));

                    // Set Granularity Candidates
                    for (GranularityExpression ge : logGranularityExpressions) {
                        switch (ge) {
                            case YEAR: {
                                yearCandidates.add(year);
                                break;
                            }
                            case MONTH: {
                                monthCandidates.add(month);
                                break;
                            }
                            case WEEK: {
                                weekCandidates.add(week);
                                break;
                            }
                            case DAY_OF_MONTH: {
                                dayOfMonthCandidates.add(dayOfMonth);
                                break;
                            }
                            case DAY_OF_WEEK: {
                                dayOfWeekCandidates.add(dayOfWeek);
                                break;
                            }
                            case HOUR_OF_DAY: {
                                hourOfDayCandidates.add(hourOfDay);
                                break;
                            }
                        }
                    }
                }
            }
        }

        // set candidates to generate Simple Calendar Patterns
        yearCandidates.add("*");
        if(monthCandidates.isEmpty() && weekCandidates.isEmpty()){
            if(logGranularityExpressions.contains(GranularityExpression.DAY_OF_MONTH)){
                monthCandidates.add("*");
            }else if(logGranularityExpressions.contains(GranularityExpression.DAY_OF_WEEK)){
                weekCandidates.add("*");
            }
        }

        addLogCandidates(yearCandidates, GranularityExpression.YEAR);
        addLogCandidates(monthCandidates, GranularityExpression.MONTH);
        addLogCandidates(weekCandidates, GranularityExpression.WEEK);
        addLogCandidates(dayOfMonthCandidates, GranularityExpression.DAY_OF_MONTH);
        addLogCandidates(dayOfWeekCandidates, GranularityExpression.DAY_OF_WEEK);
        addLogCandidates(hourOfDayCandidates, GranularityExpression.HOUR_OF_DAY);

        // Set last period
        setPeriod(period, logMonths, logWeeks, logDaysOfWeek, logDaysOfMonth, logHoursOfDay);
    }

    private void addLogCandidates(Set<String> candidates, GranularityExpression granularityExpression) {
        if (!candidates.isEmpty()) {
            candidates.add("*");
            this.logCandidates.add(new Candidates(new ArrayList<String>(candidates), granularityExpression));
        }
    }

    private void setPeriod(Period period, List<Integer> logMonths, List<Integer> logWeeks, List<Integer> logDaysOfWeek, List<Integer> logDaysOfMonth, List<Integer> logHoursOfDay) {
        Collections.sort(logMonths);
        int fromMonth = logMonths.get(0);
        int toMonth = logMonths.get(logMonths.size() -1);
        period.setFromMonth(fromMonth);
        period.setToMonth(toMonth);

        Collections.sort(logWeeks);
        int fromWeek = logWeeks.get(0);
        int toWeeks = logWeeks.get(logWeeks.size() -1);
        period.setFromWeek(fromWeek);
        period.setToWeek(toWeeks);

        Collections.sort(logDaysOfWeek);
        int fromWeekDay = logDaysOfWeek.get(0);
        int toWeekDay = logDaysOfWeek.get(logDaysOfWeek.size() -1);
        period.setFromDayOfWeek(fromWeekDay);
        period.setToDayOfWeek(toWeekDay);

        Collections.sort(logDaysOfMonth);
        int fromDayOfMonth = logDaysOfMonth.get(0);
        int toDayOfMonth = logDaysOfMonth.get(logDaysOfMonth.size() -1);
        period.setFromDayOfMonth(fromDayOfMonth);
        period.setToDayOfMonth(toDayOfMonth);

        Collections.sort(logHoursOfDay);
        int fromHour = logHoursOfDay.get(0);
        int toHour = logHoursOfDay.get(logHoursOfDay.size()-1);
        period.setFromHour(fromHour);
        period.setToHour(toHour);

        logPeriods.add(period);
    }

    private void sortEvents(List<XEvent> logEvents){
            Comparator<XEvent> compareTimestamp = (XEvent o1, XEvent o2) -> {
                Date o1Date;
                Date o2Date;
                if (o1.getAttributes().get("time:timestamp") != null) {
                    XAttribute o1da = o1.getAttributes().get("time:timestamp");
                    if (((XAttributeTimestamp) o1da).getValue() != null) {
                        o1Date = ((XAttributeTimestamp) o1da).getValue();
                    } else {
                        return -1;
                    }
                } else {
                    return -1;
                }

                if (o2.getAttributes().get("time:timestamp") != null) {
                    XAttribute o2da = o2.getAttributes().get("time:timestamp");
                    if (((XAttributeTimestamp) o2da).getValue() != null) {
                        o2Date = ((XAttributeTimestamp) o2da).getValue();
                    } else {
                        return 1;
                    }
                } else {
                    return 1;
                }

                if (o1Date == null || o1Date.toString().isEmpty()) {
                    return 1;
                } else if (o2Date == null || o2Date.toString().isEmpty()) {
                    return -1;
                } else {
                    return o1Date.compareTo(o2Date);
                }
            };
        logEvents.sort(compareTimestamp);
    }
}
