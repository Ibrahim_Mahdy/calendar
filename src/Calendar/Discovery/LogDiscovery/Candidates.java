package Calendar.Discovery.LogDiscovery;

import lombok.Data;

import java.util.List;

@Data
public class Candidates {

    private List<String> candidate;
    private GranularityExpression granularityExpression;

    public Candidates(List<String> candidate, GranularityExpression granularityExpression) {
        this.candidate = candidate;
        this.granularityExpression = granularityExpression;
    }
}
