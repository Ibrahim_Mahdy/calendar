package Calendar.Discovery.LogDiscovery;

import lombok.Data;

@Data
public class CalendarExpression {
    private String year;
    private String month;
    private String week;
    private String dayOfMonth;
    private String dayOfWeek;
    private String hour;

    public CalendarExpression() {
    }

    public CalendarExpression(String year, String month, String week, String dayOfMonth, String dayOfWeek, String hour) {
        this.year = year;
        this.month = month;
        this.week = week;
        this.dayOfMonth = dayOfMonth;
        this.dayOfWeek = dayOfWeek;
        this.hour = hour;
    }
}
