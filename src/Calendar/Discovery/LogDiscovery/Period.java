package Calendar.Discovery.LogDiscovery;

import lombok.Data;

@Data
public class Period {
    private int year;

    private int fromWeek;
    private int toWeek;

    private int fromMonth;
    private int toMonth;

    private int fromDayOfWeek;
    private int toDayOfWeek;

    private int fromDayOfMonth;
    private int toDayOfMonth;

    private int fromHour;
    private int toHour;

    public Period() {
        this.year = 0;
        this.fromWeek = 0;
        this.toWeek = 0;
        this.fromMonth = 0;
        this.toMonth = 0;
        this.fromHour = 0;
        this.toHour = 0;
    }
}
