package Calendar.Discovery;

import Calendar.Discovery.Calculation.Calculate;
import Calendar.Discovery.CalendarConstruction.*;
import Calendar.Discovery.LogDiscovery.DoubleValue;
import Calendar.Discovery.LogDiscovery.GranularityExpression;
import Calendar.Discovery.LogDiscovery.LogInfo;
import Calendar.Discovery.LogDiscovery.ParseLog;
import Calendar.Discovery.LogMetadata.CreateMetadata;
import Calendar.Discovery.SimplePatternsGeneration.EnumerationGenerator;
import Calendar.Discovery.SimplePatternsGeneration.SimpleCalendarPattern;
import org.deckfour.xes.model.XTrace;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Scanner;
import java.util.regex.Pattern;

public class Main {

    public static void main(String[] args) {
        boolean createMetadata = true;
        String filePath;
        double support;
        double confidence;

        if(args.length == 0) {
            // read log path
            System.out.println("Enter XES log path on your local computer: ");
            Scanner readInput = new Scanner(System.in);
            filePath = readInput.nextLine();

            // read Support threshold
            System.out.println("Enter Support threshold value. Support should have value between 0.1 and 1: ");
            String readSupport = readInput.nextLine();
            while (!Pattern.matches(DoubleValue.fpRegex, readSupport)) {
                System.out.println("Enter a numeric Value! Support should have value between 0.1 and 1: ");
                readSupport = readInput.nextLine();
            }
            support = Double.parseDouble(readSupport);

            // read Confidence threshold
            System.out.println("Enter Confidence threshold value. Confidence should have value between 50 and 85: ");
            String readConfidence = readInput.nextLine();
            while (!Pattern.matches(DoubleValue.fpRegex, readConfidence)){
                System.out.println("Enter a numeric Value! Confidence should have value between 50 and 85: ");
                readConfidence = readInput.nextLine();
            }
            confidence = Double.parseDouble(readConfidence);
        }else if(args.length < 3)
            throw new RuntimeException("Incorrect number of parameters");
        else{
            filePath = args[0];

            String readSupport = args[1];
            if(!Pattern.matches(DoubleValue.fpRegex, readSupport))
                throw new RuntimeException("Enter a numeric Value! Support should have value between 0.1 and 1: ");
            support = Double.parseDouble(readSupport);

            String readConfidence = args[2];
            if (!Pattern.matches(DoubleValue.fpRegex, readConfidence))
                throw new RuntimeException("Enter a numeric Value! Confidence should have value between 50 and 85: ");
            confidence = Double.parseDouble(readConfidence);
        }

        // parse log
        ParseLog parse = new ParseLog();
        File myLog = parse.readLog(filePath);
        if(myLog == null) return;
        List<XTrace> myTraces = parse.parseLog(myLog);
        if (myTraces == null) return;

        // Log Granularity to discover
        List<GranularityExpression> logGranularityExpressions = new ArrayList<>();
        logGranularityExpressions.add(GranularityExpression.DAY_OF_WEEK);
        logGranularityExpressions.add((GranularityExpression.HOUR_OF_DAY));

        // Discover log
        LogInfo logInfo = new LogInfo(myTraces, logGranularityExpressions);

        // Generate Simple Calendar Expressions
        EnumerationGenerator enumerationGenerator = new EnumerationGenerator(logInfo.getLogCandidates());
        List<SimpleCalendarPattern> simpleCalendarPatterns = enumerationGenerator.getLogSimpleCalendarPatterns();

        // Calculate Support and Confidence - Filter Simple Calendar Patterns to one Satisfy Thresholds
        Calculate calculate = new Calculate(support, confidence, logInfo.getLogCalendarExpressions(), simpleCalendarPatterns, logInfo.getLogPeriods());
        List<SimpleCalendarPattern> calculatedSP = calculate.getLogCalculatedSimpleCalendarPatterns();

        // Get hours for each day of week
        Construct construct = new Construct(calculatedSP);
        List<ConstructModel> dayOfWeekHours = construct.getDayOfWeekHours();

        // Construct Calendar of each day of week
        ConstructCalendar constructModel = new ConstructCalendar(dayOfWeekHours);
        List<CalendarModel> dayOfWeekCalendar = constructModel.getDayOfWeekCalendar();

        // Construct BIMP Calendar
        BIMPCalendar bimpCalendar = new BIMPCalendar(dayOfWeekCalendar);
        String myBIMP = bimpCalendar.getCreatedBIMPCalendar();
        System.out.println("-------------------- BIMP Calendar --------------------" + System.lineSeparator() + System.lineSeparator());
        System.out.println(myBIMP + System.lineSeparator());
        System.out.println("-------------------- BIMP Calendar --------------------" + System.lineSeparator() + System.lineSeparator());

        if (createMetadata) {
            String logName = myLog.getAbsolutePath().substring(myLog.getAbsolutePath().lastIndexOf("\\") + 1).replace(".xes", "_");
            logName += new SimpleDateFormat("yyyyMMddHHmm").format(new Date());

            StringBuilder userInput = new StringBuilder();
            userInput.append("filePath: ").append(filePath).append('\n');
            userInput.append("Support threshold: ").append(support).append('\n');
            userInput.append("Confidence threshold: ").append(confidence).append('\n');

            new CreateMetadata(logName, userInput.toString(), logGranularityExpressions, logInfo, simpleCalendarPatterns, calculatedSP, dayOfWeekHours, dayOfWeekCalendar, myBIMP);
        }

    }
}
